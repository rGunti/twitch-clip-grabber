const fs = require('fs');
const moment = require('moment');

console.debug(process.env);

// let platform = process.argv.find((v, i, a) => v.startsWith('platform='));
// if (platform) {
//     platform = platform.substring(platform.indexOf('=') + 1);
// } else {
//     platform = process.platform;
// }

let commitSha = undefined;
if ('CI_COMMIT_SHORT_SHA' in process.env) {
    commitSha = process.env['CI_COMMIT_SHORT_SHA'];
}

const package = JSON.parse(fs.readFileSync('package.json'));

let newVersion = package.version;
if (commitSha) {
    newVersion = `${newVersion}.${commitSha}`;
} else {
    newVersion = `${newVersion}.${moment().format('YYMMDDHHmmss')}`;
}

console.log(`Set package version from ${package.version} to ${newVersion}`);
package.version = newVersion;

fs.writeFileSync('package.json', JSON.stringify(package, null, '  '));
