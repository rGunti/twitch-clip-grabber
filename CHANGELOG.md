## 0.1.0-alpha3 <small>2020-07-01</small>
This **ALPHA** release updates the apps underlying framework, Angular, to the latest release version 10.
There will be no other changes in this alpha release. If you notice anything different then before, please notify me.

## 0.1.0-alpha2 <small>2020-06-30</small>
This **ALPHA** release fixes some setup issues from 0.1.0-alpha1 and adds some smaller improvements:

- Added a patch notes section to improve update communications
- Modified the sidebar to divide menu items into two sections, putting commonly used actions on top while keeping less used things on the bottom
- Added customized error messages for download errors that should help the user fix the underlying issues
- youtube-dl, the program to download clips, should now be automatically unpacked into the app's working directory
- Added customized error messages for video processing in case ffmpeg is missing from the system

_Note that you may need to reconfigure your settings since the previously present config file is now stored somewhere else._

## 0.1.0-alpha1 <small>2020-06-29</small>
*This is an **ALPHA** release, meaning this build will likely have bugs, might break without notice and probably also has some explanations and help texts missing or badly written. Please reach out to me if you find anything that is subpar.*

This is the first internal alpha release of Sir Grabbington.
Current feature set:
- List and download clips (individual or all)
- Modify downloaded clips to have same resolution (720p, individual or all)

Currently missing:
- Rendering (custom) overlay with clip name, author and date
- Settings screens / wizards

# About release stages
Here is a quick overview over the app's release stages:

- **ALPHA** builds (`-alpha`) have the very latest features but are generally untested and potentially very buggy. Features might break without notice and probably also have explanation and help texts missing or badly written. Developer features are possibly enabled to make debugging easier. If you find issues, reporting them on GitLab Issues is highly apprechiated. (Link can be found in the "About" screen)
- **BETA** builds (`-beta`) are more tested and are rolled out to more people. Generally, Beta releases don't change as much anymore but may still have some rough edges. Developer features are disabled.
- **RELEASE** builds, meaining versions without a suffix like "-alpha" etc., are generally well-tested builds that is deamed customer-ready and can be enjoyed by everyone.
- **HOTFIX** builds (`-hotfix`) fix major bugs in a release build that cause it to be unstable and potentially unusable. These hotfixes will only be released when necessary and are generally superceded by a subsequent release build. 
