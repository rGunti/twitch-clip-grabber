import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

import { HelixClipData } from 'twitch/lib/API/Helix/Clip/HelixClip';
import { SettingsService } from '../../core/services';
import { DbService } from '../../core/services/electron/db';
import { DialogService } from '../../dialogs/dialog.service';
import { ClipProcessComponent } from '../../dialogs/clip-process/clip-process.component';
import { ClipInfoDialogComponent } from '../../dialogs/clip-info-dialog/clip-info-dialog.component';

@Component({
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.scss']
})
export class ProcessComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource: MatTableDataSource<HelixClipData> = new MatTableDataSource<HelixClipData>([]);
  displayColumns: string[] = ['select', 'created_at', 'title', 'creator_name', 'actions'];
  selection: SelectionModel<HelixClipData> = new SelectionModel<HelixClipData>(true, []);

  filterValue: string;

  constructor(
    private readonly settingsService: SettingsService,
    private readonly db: DbService,
    private readonly dialog: DialogService,
    private readonly changeDef: ChangeDetectorRef
  ) { }

  async ngOnInit() {
    this.db.provideSettings(this.settingsService);
    this.db.loadAll();

    const clips = this.db.getAllClips();
    const dataSource = new MatTableDataSource<HelixClipData>(clips);
    dataSource.sort = this.sort;
    dataSource.paginator = this.paginator;

    this.dataSource = dataSource;
    this.selection = new SelectionModel<HelixClipData>(true, []);
  }

  get isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected) {
      this.selection.clear();
    } else {
      this.dataSource.data.forEach(c => this.selection.select(c))
    }
  }

  onSelectClip(clip: HelixClipData) {
    this.selection.toggle(clip);
  }

  async deleteClip(clip: HelixClipData) {
    if (confirm('Would you like to delete this clip?')) {
      this.db.deleteClip(clip.id);
      this.ngOnInit();
    }
  }

  processClip(clip: HelixClipData) {
    this.processClips([clip]);
  }

  processClips(clips: HelixClipData[]) {
    this.dialog.open(ClipProcessComponent, clips);
  }

  showClipInfo(clip: HelixClipData) {
    this.dialog.open(ClipInfoDialogComponent, clip);
  }
}
