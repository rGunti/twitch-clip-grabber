import { Component, OnInit } from '@angular/core';
import { SettingsService, AppSettings } from '../../core/services';

function camel2title(camelCase) {
  // no side-effects
  return camelCase
    // inject space before the upper case letters
    .replace(/([A-Z])/g, function(match) {
       return " " + match;
    })
    // replace first char with upper case
    .replace(/^./, function(match) {
      return match.toUpperCase();
    });
}

@Component({
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor(
    public settings: SettingsService
  ) { }

  ngOnInit(): void {
    this.settings.loadSettings();
  }

  get keys(): string[] {
    return this.settings.getSettingKeys();
  }

  getKeyString(key: string): string {
    return camel2title(key);
  }

  getValue(key: string): any {
    return this.settings.settings[key];
  }

}
