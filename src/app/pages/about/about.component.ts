import { Component, OnInit } from '@angular/core';
import { ElectronService } from '../../core/services';
import { AppInfoService } from '../../core/services/app-info.service';

export class LibraryLicenseInformation {
  constructor(
    public library: string,
    public author: string,
    public license: string,
    public url: string
  ) {}
}

@Component({
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(
    private readonly electronService: ElectronService,
    public readonly appInfo: AppInfoService
  ) { }

  ngOnInit(): void {
  }

  openUrl(url: string) {
    this.electronService.openUrl(url);
  }

  get libraries(): LibraryLicenseInformation[] {
    return [
      new LibraryLicenseInformation('Angular', 'Google LLC', 'MIT', 'http://angular.io/license'),
      new LibraryLicenseInformation('Electron', 'GitHub Inc.', 'MIT', 'https://github.com/electron/electron/blob/master/LICENSE'),
      new LibraryLicenseInformation('open', 'Sindre Sorhus', 'MIT', 'https://github.com/sindresorhus/open/blob/master/license'),
      new LibraryLicenseInformation('rxjs', 'Google, Inc., Netflix, Inc., Microsoft Corp. and contributors', 'Apache License 2.0', 'https://github.com/ReactiveX/rxjs/blob/master/LICENSE.txt'),
      new LibraryLicenseInformation('typescript', 'Microsoft Corp.', 'Apache License 2.0', 'https://github.com/microsoft/TypeScript/blob/master/LICENSE.txt'),
      new LibraryLicenseInformation('angular-electron Template', 'maximegris', 'MIT', 'https://github.com/maximegris/angular-electron/blob/master/LICENSE.md')
    ];
  }

}
