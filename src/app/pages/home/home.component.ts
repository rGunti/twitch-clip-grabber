import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { TwitchService, ProcessReport, SettingsService } from '../../core/services';
import { DbService } from '../../core/services/electron/db';
import { HelixClip } from 'twitch';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

import { retry, throttleTime } from 'rxjs/operators';
import { Observable, concat } from 'rxjs';
import { DialogService } from '../../dialogs/dialog.service';
import { DownloadErrorHelpDialogComponent } from '../../dialogs/download-error-help-dialog/download-error-help-dialog.component';

export interface DownloadStats {
  [key: string]: ProcessReport;
}

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource: MatTableDataSource<HelixClip> = new MatTableDataSource<HelixClip>([]);
  displayColumns: string[] = ['select', 'creationDate', 'title', 'creatorDisplayName', 'downloadStatus'];
  selection: SelectionModel<HelixClip> = new SelectionModel<HelixClip>(true, []);

  filterValue: string;

  downloadStats: DownloadStats = {};

  constructor(
    private twitch: TwitchService,
    private settingsService: SettingsService,
    private db: DbService,
    private dialog: DialogService,
    private change: ChangeDetectorRef
  ) { }

  async ngOnInit() {
    this.settingsService.loadSettings();
    this.db.provideSettings(this.settingsService);
    this.db.loadAll();

    const settings = this.settingsService.settings;
    this.twitch.initializeClient(settings.twitchClientId, settings.twitchClientSecret);

    const user = await this.twitch.getUser(settings.twitchChannel);
    if (user) {
      const clips = await this.twitch.getClips(user.id);
      this.dataSource = new MatTableDataSource<HelixClip>(clips);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.dataSource.filterPredicate = (data: HelixClip, filter: string) => {
        filter = filter.toLowerCase();
        return data.id.toLowerCase() === filter
          || data.title.toLowerCase().indexOf(filter) >= 0
          || data.creatorDisplayName.toLowerCase().indexOf(filter) >= 0
      };
      this.selection.clear();

      this.selection = new SelectionModel<HelixClip>(true, []);
    }
  }

  get isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected) {
      this.selection.clear();
    } else {
      this.dataSource.data.forEach(c => this.selection.select(c))
    }
  }

  onSelectClip(clip: HelixClip) {
    this.selection.toggle(clip);
  }

  applyFilter(filter: string, skipAssignment: boolean = false) {
    if (!skipAssignment) {
      this.filterValue = filter;
    }
    this.dataSource.filter = this.filterValue.trim();
  }

  hasDownloadStats(videoId: string): boolean {
    return this.downloadStats.hasOwnProperty(videoId);
  }

  getDownloadStats(videoId: string): ProcessReport {
    if (this.hasDownloadStats(videoId)) {
      return this.downloadStats[videoId];
    }
    return null;
  }

  private prepareClipDownload(clip: HelixClip): Observable<ProcessReport> {
    return this.twitch.downloadClip(clip, this.settingsService.settings.storagePath)
      .pipe(
        retry(3),
        //throttleTime(200)
      );
  }

  hasClip(clip: HelixClip): boolean {
    return this.db.hasClip(clip);
  }

  downloadClip(clip: HelixClip) {
    this.downloadStats[clip.id] = {
      status: 'Initializing ...'
    };

    this.prepareClipDownload(clip)
      .subscribe(r => {
        console.log('New Stat info', r);
        this.downloadStats[clip.id] = r;
        this.change.detectChanges();
      }, e => {
        this.downloadStats[clip.id] = {
          status: 'Failed!',
          error: e.message
        };
        this.change.detectChanges();
      }, () => {
        this.db.addClip(clip['_data']);
        delete this.downloadStats[clip.id];
        this.change.detectChanges();
      });
  }

  downloadSelectedClips() {
    const clipTasks = this.selection.selected.map(c => this.prepareClipDownload(c));
    for (const clip of this.selection.selected) {
      this.downloadStats[clip.id] = {
        status: 'Waiting for download …'
      };
    }
    concat(...clipTasks)
      .subscribe(r => {
        console.log(r);
        this.downloadStats[r.sourceId] = r;
        if (r.completed) {
          const existingClips = this.selection.selected.filter(c => c.id === r.sourceId);
          if (existingClips && existingClips.length >= 1) {
            this.selection.deselect(...existingClips);
            for (const clip of existingClips) {
              this.db.addClip(clip['_data']);
            }
          }
          delete this.downloadStats[r.sourceId];
        }
        this.change.detectChanges();
      }, e => {
        console.log(e);
        this.change.detectChanges();
      }, () => {
        this.change.detectChanges();
      });
  }

  onOpenErrorInfoDialog(clipId: string) {
    const error = this.getDownloadStats(clipId).error;
    this.dialog.open(DownloadErrorHelpDialogComponent, error,
      false, null, null, null, null, '75vh');
  }

}
