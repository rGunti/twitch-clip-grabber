import { Injectable } from '@angular/core';
import { version as packageVersion } from '../../../../package.json';
import { AppConfig } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppInfoService {

  constructor() { }

  get version(): string {
    return packageVersion;
  }

  get isProdBuild(): boolean {
    return AppConfig.production;
  }
  get currentEnvironment(): string {
    return AppConfig.environment;
  }
}
