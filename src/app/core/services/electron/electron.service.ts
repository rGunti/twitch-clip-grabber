import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote, BrowserWindow } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import * as process from 'process';

export enum Platform {
  Unsupported,
  Windows,
  macOS,
  Linux,
  Browser
}

@Injectable({
  providedIn: 'root'
})
export class ElectronService {
  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  fs: typeof fs;
  path: typeof path;
  process: typeof process;

  private readonly platform: string = undefined;

  get isElectron(): boolean {
    return !!(window && window.process && window.process.type);
  }

  get currentPlatform(): Platform {
    if (this.isElectron) {
      switch (this.platform) {
        case 'win32': return Platform.Windows;
        case 'darwin': return Platform.macOS;
        case 'linux': return Platform.Linux;
        default: return Platform.Unsupported;
      }
    }
    return Platform.Browser;
  }

  isOnPlatform(...platform: Platform[]): boolean {
    return platform.indexOf(this.currentPlatform) >= 0;
  }

  constructor() {
    // Conditional imports
    if (this.isElectron) {
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;
      this.remote = window.require('electron').remote;

      this.childProcess = window.require('child_process');
      this.fs = window.require('fs');
      this.path = window.require('path');
      this.process = window.require('process');

      this.platform = this.process.platform;

      this.initializeImports();
    }
  }

  protected initializeImports() {
  }

  get focusedBrowserWindow(): BrowserWindow {
    return this.isElectron ? this.remote.BrowserWindow.getFocusedWindow() : null;
  }

  toggleDeveloperConsole() {
    if (this.isElectron) {
      const webContent = this.remote.webContents.getFocusedWebContents();
      if (webContent) {
        webContent.toggleDevTools();
      }
    }
  }

  openUrl(url: string) {
    if (this.isElectron) {
      this.remote.shell.openExternal(url);
    }
  }
}
