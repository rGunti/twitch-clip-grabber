import { Injectable } from '@angular/core';
import { ElectronService, Platform } from './electron.service';
import { SettingsService } from './settings.service';
import { Observable } from 'rxjs';

import * as ffmpeg from 'fluent-ffmpeg';
import { finalize } from 'rxjs/operators';


export interface FfmpegRenderStats {
  frames: number;
  currentFps: number;
  currentKbps: number;
  targetSize: number;
  timemark: string;
  percent: number;
}

export class RenderProgress {
  constructor(
    public readonly stats: FfmpegRenderStats,
    public readonly statusText: string = null,
    public readonly error: string | Error = null
  ) {
  }
}


@Injectable({
  providedIn: 'root'
})
export class FfmpegService extends ElectronService {

  ffmpeg: typeof ffmpeg;
  settings: SettingsService;

  get ffmpegPath(): string {
    return this.isOnPlatform(Platform.Windows)
      ? 'ffmpeg.exe'
      : 'ffmpeg';
  }
  get ffprobePath(): string {
    return this.isOnPlatform(Platform.Windows)
      ? 'ffprobe.exe'
      : 'ffprobe';
  }

  constructor() {
    super();
  }

  protected initializeImports() {
    console.log('Running FFMPEG imports ...');
    this.ffmpeg = window.require('fluent-ffmpeg');
    const dummyCmd = this.ffmpeg();
    dummyCmd.setFfmpegPath(this.ffmpegPath);
    dummyCmd.setFfprobePath(this.ffprobePath);
  }

  initializeSettings(settings: SettingsService) {
    this.settings = settings;
  }

  getMetaData(file: string): Observable<ffmpeg.FfprobeData> {
    return new Observable<ffmpeg.FfprobeData>(o => {
      this.ffmpeg.ffprobe(file, (err, data) => {
        if (err) {
          o.error(err);
        } else {
          o.next(data);
          o.complete();
        }
      });
    });
  }

  rescale(file: string, width: number, height: number): Observable<RenderProgress> {
    const tmpFile = `${file}.TMP`;
    return this.runFfmpegProcess(
      this.ffmpeg(file)
        .size(`${width}x${height}`)
        .output(tmpFile)
        .outputFormat('mp4')
    ).pipe(
      finalize(() => {
        if (this.fs.existsSync(tmpFile)) {
          this.fs.unlinkSync(file);
          this.fs.renameSync(tmpFile, file);
        }
      })
    );
  }

  private runFfmpegProcess(process: ffmpeg.FfmpegCommand): Observable<RenderProgress> {
    return new Observable(o => {
      const stderr = [];
      process
        .on('start', cmd => {
          o.next(new RenderProgress(null, 'Initializing ...'));
        })
        .on('progress', (stats: FfmpegRenderStats) => {
          if (stats) {
            o.next(new RenderProgress(stats, 'Processing ...'));
          }
        })
        .on('stderr', (i) => {
          stderr.push(i);
        })
        .on('error', err => {
          o.next(new RenderProgress(null, 'Failed to process', err));
          o.error(err);
        })
        .on('end', () => {
          o.complete();
        })
        .run();
    });
  }
}
