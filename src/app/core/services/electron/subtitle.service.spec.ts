import { TestBed } from '@angular/core/testing';

import { SubtitleService } from './subtitle.service';

describe('SubtitleService', () => {
  let service: SubtitleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubtitleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should deliver correct color values', () => {
    const colorValues = {
      '#000000': '000000',
      '#ff00ff': 'ff00ff',
      '#abcdef': 'efcdab'
    };
    for (const source of Object.keys(colorValues)) {
      expect(service.convertHexToAssColor(source)).toBe(colorValues[source]);
    }
  })
});
