import { Injectable } from '@angular/core';
import { ElectronService, Platform } from './electron.service';

import TwitchClient, { HelixUser, HelixClip } from 'twitch';
import * as youtubedl from 'youtube-dl';
import { Observable } from 'rxjs';
import { ProcessReport } from './events';
import { SettingsService } from './settings.service';

@Injectable({
  providedIn: 'root'
})
export class TwitchService extends ElectronService {

  twitch: typeof TwitchClient;
  client: TwitchClient;

  ytdl: typeof youtubedl;

  get ytdlPath(): string {
    const filename = this.isOnPlatform(Platform.Windows)
      ? 'youtube-dl.exe'
      : 'youtube-dl';
    if (this.path) {
      return this.path.join(this.path.join(this.process.cwd(), filename));
    }
    return filename;
  }

  constructor() {
    super();
  }

  protected initializeImports() {
    console.log('Running Twitch Imports ...');
    this.twitch = window.require('twitch').default;
    this.ytdl = window.require('youtube-dl');
    if (this.ytdl) {
      const ytdlPath = this.ytdlPath;
      console.info('youtube-dl is expected here:', ytdlPath);
      if (!this.fs.existsSync(ytdlPath)) {
        this.fs.copyFileSync(this.ytdl.getYtdlBinary(), ytdlPath);
      }
      if (!this.checkYoutubeDl()) {
        console.warn('Trying another path for youtube-dl ...', ytdlPath);
        this.ytdl.setYtdlBinary(ytdlPath);
      }
      if (!this.checkYoutubeDl()) {
        console.error('Could not find youtube-dl!')
      }
    }
  }

  private checkYoutubeDl(): boolean {
    const path = this.ytdl.getYtdlBinary();
    console.log('youtube-dl Path: ', path);
    return this.fs.existsSync(this.ytdl.getYtdlBinary());
  }

  initializeClient(clientId: string, clientSecret: string) {
    console.log('Initializing Twitch Client with credentials:',
      clientId, clientSecret);
    this.client = this.twitch.withClientCredentials(clientId, clientSecret);
  }

  getUser(username: string): Promise<HelixUser> {
    return this.client.helix.users.getUserByName(username);
  }

  async getClips(userId: string): Promise<HelixClip[]> {
    return this.client.helix.clips
      .getClipsForBroadcasterPaginated(userId)
      .getAll();
  }

  downloadClip(clip: HelixClip, destinationFolder: string): Observable<ProcessReport> {
    return new Observable<ProcessReport>(o => {
      const videoPath = this.path.join(destinationFolder, `${clip.id}.mp4`);
      this.ytdl.setYtdlBinary(this.ytdlPath);
      const video = this.ytdl(clip.url, [], {});

      if (!this.fs.existsSync(destinationFolder)) {
        this.fs.mkdirSync(destinationFolder);
      }

      let size = 0;
      video.on('info', (info: {size: number}) => {
        size = info.size;
        o.next({
          percentage: -1,
          sourceId: clip.id,
          status: `Initializing download ...`
        });
      });
      
      let pos = 0;
      video.on('data', (buffer: any) => {
        if (buffer.length) {
          pos += buffer.length;
          const percent = (pos / size * 100);

          o.next({
            status: `Downloading clip "${clip.title}" by ${clip.creatorDisplayName} ...`,
            sourceId: clip.id,
            percentage: percent
          });
        }
      });
      video.on('error', (e) => {
        console.log('Download failed', e);
        o.error(e);
      });
      video.on('end', (i) => {
        console.log('Download completed');
        o.next({
          status: `Downloading clip "${clip.title}" by ${clip.creatorDisplayName} completed`,
          sourceId: clip.id,
          percentage: 100,
          completed: true
        });
        o.complete();
      });

      console.log('Downloading to', videoPath);
      video.pipe(this.fs.createWriteStream(videoPath));
    });
  }

}
