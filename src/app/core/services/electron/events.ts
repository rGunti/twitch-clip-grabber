export interface ProcessReport {
    status: string;
    sourceId?: string;
    percentage?: number;
    error?: string;
    completed?: boolean;
}