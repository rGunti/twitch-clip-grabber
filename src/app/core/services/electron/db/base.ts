export interface KeyValueDir<T> {
    [id: string]: T;
}

export interface IDatabaseSet<T> {
    getAll(): T[];
    add(item: T): void;
    addRange(items: T[]): void;
    delete(item: T): void;
}
export interface IKeyedDatabaseSet<T> extends IDatabaseSet<T> {
    get(id: string): T;
    has(id: string): boolean;
    deleteById(id: string): void;
}

export declare type DbSetFetcher<TSet> = (db: TSet) => TSet;
export declare type ArrayDbSetFetcher<TEntity> = DbSetFetcher<TEntity[]>;
export declare type ObjectDbSetFetcher<TSet, TEntity> = (db: TSet) => KeyValueDir<TEntity>;

export declare type IdGetter<T> = (item: T) => string;

export class ArrayDatabaseSet<T> implements IDatabaseSet<T> {
    constructor(
        private db: any,
        private dbSet: ArrayDbSetFetcher<T>,
        private saveAction?: (self: ArrayDatabaseSet<T>) => void
    ) {
    }

    private save() {
        if (this.saveAction) {
            this.saveAction(this);
        }
    }

    public getAll(): T[] {
        return this.dbSet(this.db);
    }

    public add(item: T) {
        this.dbSet(this.db).push(item);
        this.save();
    }
    public addRange(items: T[]) {
        this.dbSet(this.db).push(...items);
        this.save();
    }
    public delete(item: T): void {
        const set = this.dbSet(this.db);
        const i = set.indexOf(item);
        if (i >= 0) {
            set.splice(i, 1);
            this.save();
        }
    }
}

export class ObjectDatabaseSet<TSet, TEntity> implements IKeyedDatabaseSet<TEntity> {
    constructor(
        private db: any,
        private objSet: ObjectDbSetFetcher<TSet, TEntity>,
        private idGetter: IdGetter<TEntity>,
        private saveAction?: (self: ObjectDatabaseSet<TSet, TEntity>) => void
    ) {
    }

    private save() {
        if (this.saveAction) {
            this.saveAction(this);
        }
    }

    get(id: string): TEntity {
        return this.objSet(this.db)[id];
    }
    getAll(): TEntity[] {
        return Object.values(this.objSet(this.db));
    }
    add(item: TEntity, skipSave?: boolean) {
        this.objSet(this.db)[this.idGetter(item)] = item;
        if (!skipSave) {
            this.save();
        }
    }
    addRange(items: TEntity[]) {
        items.forEach(i => this.add(i, true));
        this.save();
    }

    has(id: string): boolean {
        return Object.keys(this.objSet(this.db)).includes(id);
    }

    delete(item: TEntity): void {
        this.deleteById(this.idGetter(item));
    }
    deleteById(id: string): void {
        delete this.objSet(this.db)[id];
        this.save();
    }
    
}

export abstract class Database<T> {
    protected db: T;

    constructor(
        defaultDb?: T,
        loadOnConstruct: boolean = true
    ) {
        if (defaultDb) {
            this.db = defaultDb;
        }
        if (loadOnConstruct) {
            this.load();
        }
    }

    public abstract load(): void;
    public abstract save(): void;
}

export declare type LoadDatabaseCallback<TDatabase extends Database<TDatabaseContent>, TDatabaseContent> = (db: TDatabase) => TDatabaseContent;
export declare type SaveDatabaseCallback<TDatabase extends Database<TDatabaseContent>, TDatabaseContent> = (db: TDatabase, dbContent: TDatabaseContent) => void;
export declare type CreationCondition<TDatabase extends Database<TDatabaseContent>, TDatabaseContent> = (db: TDatabase) => boolean;

export class CallbackEnabledDatabase<T, TSelf extends CallbackEnabledDatabase<T, TSelf>> extends Database<T> {
    protected loadCallback: LoadDatabaseCallback<TSelf, T>;
    protected saveCallback: SaveDatabaseCallback<TSelf, T>;
    protected creationCondition: CreationCondition<TSelf, T>;
    
    constructor(
        loadCallback: LoadDatabaseCallback<TSelf, T>,
        saveCallback: SaveDatabaseCallback<TSelf, T>,
        creationCondition: CreationCondition<TSelf, T>,
        defaultDb?: T
    ) {
        super(defaultDb, false);
        this.loadCallback = loadCallback;
        this.saveCallback = saveCallback;
        this.creationCondition = creationCondition;
    }

    public load() {
        if (this.creationCondition(this as any as TSelf)) {
            this.save();
        }
        this.db = this.loadCallback(this as any as TSelf);
    }

    public save() {
        this.saveCallback(this as any as TSelf, this.db);
    }

    public getArraySet<TEntity>(fetch: ArrayDbSetFetcher<TEntity>) {
        return new ArrayDatabaseSet<TEntity>(this.db, fetch, () => this.save());
    }

    public getObjectSet<TEntity>(
        o: ObjectDbSetFetcher<T, TEntity>,
        id: IdGetter<TEntity> = (i) => i['id']
    ): IKeyedDatabaseSet<TEntity> {
        return new ObjectDatabaseSet<T, TEntity>(
            this.db, o, id, () => this.save());
    }
}
