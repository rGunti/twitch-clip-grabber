import { Injectable } from '@angular/core';

import { ElectronService } from '../electron.service';
import { SettingsService } from '../settings.service';
import { GrabbingtonDatabase, GrabbingtonSchema } from './grabbington';
import HelixClip, { HelixClipData } from 'twitch/lib/API/Helix/Clip/HelixClip';

const DB_FILE = '_db.json';

@Injectable({
  providedIn: 'root'
})
export class DbService extends ElectronService {

  private db: GrabbingtonDatabase;

  constructor(
    private settings: SettingsService
  ) {
    super();
    this.db = new GrabbingtonDatabase(
      (db) => {
        const dbPath = this.getDbFilePath();
        console.log('Reading database from file …', dbPath);
        const dbContent = JSON.parse(this.fs.readFileSync(dbPath).toString()) as GrabbingtonSchema;
        if (dbContent) {
          if (dbContent.processedClips === undefined || dbContent.processedClips === null) {
            dbContent.processedClips = [];
          }
        }
        return dbContent;
      },
      (db, dbContent) => {
        const dbPath = this.getDbFilePath();
        console.log('Writing database to file …', dbPath);
        this.fs.writeFileSync(this.getDbFilePath(), JSON.stringify(dbContent, null, '\t'));
      },
      (db) => !this.fs.existsSync(this.getDbFilePath())
    );
  }

  private getDbFilePath(): string {
    return this.path.join(this.settings.settings.storagePath, DB_FILE);
  }

  protected initializeImports() {
  }

  public provideSettings(settings: SettingsService) {
    this.settings = settings;
  }

  public loadAll() {
    this.settings.loadSettings();
    this.db.load();
  }

  public getClip(id: string): HelixClipData {
    return this.db.clips.get(id);
  }
  public getAllClips(): HelixClipData[] {
    return this.db.clips.getAll();
  }
  public addClip(clip: HelixClipData) {
    this.db.clips.add(clip);
  }
  public hasClip(clip: HelixClipData|HelixClip): boolean {
    return this.db.clips.has(clip.id);
  }
  public deleteClip(clipId: string) {
    this.db.clips.deleteById(clipId);
    this.deleteClipFile(clipId);
  }

  private deleteClipFile(clipId: string) {
    this.fs.unlinkSync(this.path.join(this.settings.settings.storagePath, `${clipId}.mp4`));
  }
}
