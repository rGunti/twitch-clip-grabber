import { KeyValueDir, CallbackEnabledDatabase, LoadDatabaseCallback, SaveDatabaseCallback, IKeyedDatabaseSet, CreationCondition, IDatabaseSet, ArrayDatabaseSet } from "./base";

import { HelixClipData } from 'twitch/lib/API/Helix/Clip/HelixClip';

export interface GrabbingtonSchema {
    version: number;
    created: Date;
    clips: KeyValueDir<HelixClipData>;
    processedClips: string[];
}

export class GrabbingtonDatabase extends CallbackEnabledDatabase<GrabbingtonSchema, GrabbingtonDatabase> {
    constructor(
        loadCallback: LoadDatabaseCallback<GrabbingtonDatabase, GrabbingtonSchema>,
        saveCallback: SaveDatabaseCallback<GrabbingtonDatabase, GrabbingtonSchema>,
        creationCondition: CreationCondition<GrabbingtonDatabase, GrabbingtonSchema>
    ) {
        super(
            loadCallback, saveCallback, creationCondition,
            {
                version: 1,
                created: new Date(),
                clips: {},
                processedClips: []
            }
        );
    }

    get clips(): IKeyedDatabaseSet<HelixClipData> {
        return this.getObjectSet(s => s.clips);
    }

    get processedClips(): IDatabaseSet<string> {
        return this.getArraySet(() => this.db.processedClips);
    }
}
