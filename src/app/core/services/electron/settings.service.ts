import { Injectable } from '@angular/core';
import { ElectronService } from './electron.service';

const SETTINGS_FILE = 'settings.json';

export interface AppSettings {
  storagePath: string;
  twitchClientId: string;
  twitchClientSecret: string;
  twitchChannel: string;
}

@Injectable({
  providedIn: 'root'
})
export class SettingsService extends ElectronService {

  private _settings: AppSettings = {
    storagePath: './data',
    twitchClientId: null,
    twitchClientSecret: null,
    twitchChannel: null
  };

  constructor() {
    super();
  }

  protected initializeImports() {
  }

  get settings(): AppSettings {
    return this._settings;
  }

  get fileExists(): boolean {
    return this.fs.existsSync(SETTINGS_FILE);
  }

  loadSettings() {
    if (!this.fileExists) {
      console.log('Settings file doesn\'t exist yet');
      this.saveSettings();
    }

    try {
      console.log('Reading settings file ...');
      const fileContent = this.fs.readFileSync(SETTINGS_FILE);
      const obj = JSON.parse(fileContent.toString());

      console.log('Settings loaded:', obj);
      this._settings = obj as AppSettings;
    } catch (e) {
      console.error('Failed to load settings file!', e);
    }
  }

  saveSettings() {
    console.log('Writing Settings to file ...');
    this.fs.writeFileSync(SETTINGS_FILE, JSON.stringify(this._settings, null, '\t'));
  }

  /** Returns all setting keys loaded */
  getSettingKeys(): string[] {
    return Array.from(Object.getOwnPropertyNames(this._settings));
  }
}
