import { Injectable } from '@angular/core';
import * as assStringify from 'ass-stringify';

/** from ass-stringify */
export interface AssSection {
  section: 'Script Info' | 'V4 Styles' | 'Events' | string;
  body: AssObject[];
}
/** from ass-stringify */
export interface AssObject {
  type?: string;
  key?: string;
  value: any;
}

function assSimpleValue(key: string, value: string): AssObject {
  return { key, value };
}
function assComment(value: string): AssObject {
  return { type: 'comment', value };
}
function assBoolean(value: boolean): string {
  return value ? '-1' : '0';
}
function assTimestamp(value: number): string {
  const h = Math.floor(value / 60 / 60);
  const m = Math.floor(value / 60) % 60;
  const s = Math.floor(value % 60);
  return `${h}:${m.toString().padStart(2, '0')}:${s.toFixed(2)}`;
}

/* *** Custom Typings *** */
export declare type Colour = string;
export interface SubtitleScript {
  scriptInfo: SubtitleScriptInfo[];
  styles: SubtitleStyle[];
  events: SubtitleScriptEvent[];
}
export interface SubtitleScriptInfo {
  key: string;
  value: string;
}
export enum SubtitleBorderStyle {
  OutlineAndDropShadow = '1',
  OpaqueBox = '3'
}
export enum SubtitleAlignmentStyle {
  BottomLeft = 1,
  BottomCenter = 2,
  BottomRight = 3,
  MiddleLeft = 4,
  MiddleCenter = 5,
  MiddleRight = 6,
  TopLeft = 7,
  TopCenter = 8,
  TopRight = 9
}
export interface SubtitleStyle {
  name: string;
  fontName: string;
  fontSize: number;
  primaryColour: Colour;
  secondaryColour: Colour;
  outlineColour: Colour;
  backColour: Colour;
  bold: boolean;
  italic: boolean;
  borderStyle: SubtitleBorderStyle;
  outline: number;
  shadow: number;
  alignment: number;
  marginL: number;
  marginR: number;
  marginV: number;
  alphaLevel: number;
  encoding: number;
}
export interface SubtitleScriptEvent {
  type: string;
  marked: boolean;
  start: number;
  end: number;
  style: string;
  name: string;
  marginL: number;
  marginR: number;
  marginV: number;
  effect: string;
  text: string;
}

@Injectable({
  providedIn: 'root'
})
export class SubtitleService {

  constructor() { }

  produceAss(content: AssSection[]): string {
    return assStringify(content);
  }

  convertHexToAssColor(color: Colour, includeAlpha: boolean = false): string {
    if (color.startsWith('#')) {
      color = color.substring(1);
    }
    // 0x654321 = 6636321 = "214365"
    const colorValue = Number.parseInt(color, 16);

    const EXP_R = includeAlpha ? 8 : 6;
    const EXP_G = includeAlpha ? 6 : 4;
    const EXP_B = includeAlpha ? 4 : 2;
    const EXP_A = includeAlpha ? 2 : 0;

    const r = Math.floor(colorValue % Math.pow(16, EXP_R) / Math.pow(16, EXP_G));
    const g = Math.floor(colorValue % Math.pow(16, EXP_G) / Math.pow(16, EXP_B));
    const b = Math.floor(colorValue % Math.pow(16, EXP_B) / Math.pow(16, EXP_A));
    const a = Math.floor(colorValue % Math.pow(16, EXP_A) / Math.pow(16, 0));

    return (includeAlpha ? `${a.toString(16).padStart(2, '0')}` : '')
      + `${b.toString(16).padStart(2, '0')}${g.toString(16).padStart(2, '0')}${r.toString(16).padStart(2, '0')}`;
  }

  convertToAss(script: SubtitleScript): AssSection[] {
    const ass: AssSection[] = [];
    
    ass.push({
      section: 'Script Info',
      body: script.scriptInfo.map(i => {
        return { key: i.key, value: i.value };
      })
    });
    ass[0].body.splice(0, 0, assComment('Generated by Sir Grabbington'));

    ass.push({
      section: 'V4 Styles',
      body: script.styles.map(s => {
        return {
          key: 'Style',
          value: {
            Name: s.name,
            Fontname: s.fontName,
            Fontsize: `${s.fontSize}`,
            PrimaryColour: this.convertHexToAssColor(s.primaryColour),
            SecondaryColour: this.convertHexToAssColor(s.secondaryColour),
            TertiaryColour: this.convertHexToAssColor(s.outlineColour),
            BackColour: this.convertHexToAssColor(s.backColour, true),
            Bold: assBoolean(s.bold),
            Italic: assBoolean(s.italic),
            BorderStyle: `${s.borderStyle}`,
            Outline: `${s.outline}`,
            Shadow: `${s.shadow}`,
            Alignment: `${s.alignment}`,
            MarginL: `${s.marginL}`,
            MarginR: `${s.marginR}`,
            MarginV: `${s.marginV}`,
            AlphaLevel: `${s.alphaLevel}`,
            Encoding: `${s.encoding}`
          }
        };
      })
    });
    ass[1].body.splice(0, 0, {
      key: 'Format',
      value: [
        "Name",
        "Fontname",
        "Fontsize",
        "PrimaryColour",
        "SecondaryColour",
        "TertiaryColour",
        "BackColour",
        "Bold",
        "Italic",
        "BorderStyle",
        "Outline",
        "Shadow",
        "Alignment",
        "MarginL",
        "MarginR",
        "MarginV",
        "AlphaLevel",
        "Encoding"
      ]
    });

    ass.push({
      section: 'Events',
      body: script.events.map(e => {
        return {
          key: e.name,
          value: {
            Marked: `Marked=${e.marked ? 1 : 0}`,
            Start: assTimestamp(e.start),
            End: assTimestamp(e.end),
            Style: e.style || 'Default',
            Name: e.name,
            MarginL: e.marginL.toString().padStart(3, '0'),
            MarginR: e.marginR.toString().padStart(3, '0'),
            MarginV: e.marginV.toString().padStart(3, '0'),
            Effect: e.effect,
            Text: e.text
          }
        };
      })
    });
    ass[2].body.splice(0, 0, {
      key: 'Format',
      value: [
        "Marked",
        "Start",
        "End",
        "Style",
        "Name",
        "MarginL",
        "MarginR",
        "MarginV",
        "Effect",
        "Text"
      ]
    });

    return ass;
  }
}
