export interface KeyedEntity<TKey> {
    id: TKey;
}
export interface StringKeyedEntity extends KeyedEntity<string> {}
