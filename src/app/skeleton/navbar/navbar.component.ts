import { Component, OnInit } from '@angular/core';
import { AppConfig } from '../../../environments/environment';
import { ElectronService } from '../../core/services';
import { DialogService } from '../../dialogs/dialog.service';
import { PatchNotesDialogComponent } from '../../dialogs/patch-notes-dialog/patch-notes-dialog.component';

export interface NavbarLink {
  link?: string;
  title?: string;
  icon?: string;
  action?: () => any;
  divider?: boolean;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  links: NavbarLink[] = [
    { link: 'home', icon: 'home', title: 'Home' },
    { link: 'process', icon: 'movie_filter', title: 'Process Clips' }
  ];
  bottomLinks: NavbarLink[] = [
    { icon: 'description', title: 'Patch Notes', action: () => { this.openPatchNotes() } },
    { divider: true },
    { link: 'settings', icon: 'settings', title: 'Settings' },
    { link: 'about', icon: 'info', title: 'About' }
  ];
  isDev = !AppConfig.production;

  constructor(
    public readonly electron: ElectronService,
    private readonly dialog: DialogService
  ) { }

  ngOnInit(): void {
  }

  openPatchNotes() {
    this.dialog.open(PatchNotesDialogComponent, null, false, undefined, undefined, '70vw', undefined, '70vw', undefined);
  }

}
