import { Component } from '@angular/core';
import { ElectronService, Platform } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { AppInfoService } from './core/services/app-info.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,
    public readonly appInfo: AppInfoService
  ) {
    translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);
  }

  get showWindowControls() {
    return this.electronService.isOnPlatform(Platform.Windows, Platform.Linux, Platform.Unsupported);
  }

  minimize() {
    const window = this.electronService.focusedBrowserWindow;
    window.minimize();
  }

  maximize() {
    const window = this.electronService.focusedBrowserWindow;
    if (window.isMaximized()) {
      window.restore();
    } else {
      window.maximize();
    }
  }

  close() {
    const window = this.electronService.focusedBrowserWindow;
    window.close();
  }
}
