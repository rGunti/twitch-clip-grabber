import { Component, OnInit } from '@angular/core';
import { AppInfoService } from '../../core/services/app-info.service';
import { ElectronService } from '../../core/services';
// tslint:disable-next-line
import { CHANGELOG } from '../../shared/generated/changelog';

@Component({
  templateUrl: './patch-notes-dialog.component.html',
  styleUrls: ['./patch-notes-dialog.component.css']
})
export class PatchNotesDialogComponent implements OnInit {

  changeLogContent: string;

  constructor(
    public readonly appInfo: AppInfoService,
    electron: ElectronService
  ) {
    this.changeLogContent = CHANGELOG;
    if (!this.changeLogContent) {
      this.changeLogContent = this.load(electron, 'CHANGELOG.md');
    }
    if (!this.changeLogContent) {
      this.changeLogContent = '## Failed to load change log :(';
    }
  }

  private load(electron: ElectronService, file: string): string {
    try {
      const changeLog = electron.fs.readFileSync(file);
      return changeLog.toString();
    } catch (e) {
      console.warn(e);
      return null;
    }
  }

  ngOnInit(): void {
  }

}
