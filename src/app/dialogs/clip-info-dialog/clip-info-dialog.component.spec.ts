import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipInfoDialogComponent } from './clip-info-dialog.component';

describe('ClipInfoDialogComponent', () => {
  let component: ClipInfoDialogComponent;
  let fixture: ComponentFixture<ClipInfoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClipInfoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
