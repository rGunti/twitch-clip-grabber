import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HelixClipData } from 'twitch/lib/API/Helix/Clip/HelixClip';
import { Observable } from 'rxjs';
import { FfmpegService, SettingsService } from '../../core/services';
import { finalize, tap } from 'rxjs/operators';
import * as ffmpeg from 'fluent-ffmpeg';
import * as moment from 'moment';

@Component({
  templateUrl: './clip-info-dialog.component.html',
  styleUrls: ['./clip-info-dialog.component.scss']
})
export class ClipInfoDialogComponent implements OnInit {

  isLoading: boolean = true;
  videoInfo: ffmpeg.FfprobeData;

  constructor(
    @Inject(MAT_DIALOG_DATA) public clip: HelixClipData,
    private readonly ffmpegService: FfmpegService,
    private readonly settingsService: SettingsService,
    private readonly changeDetection: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.settingsService.loadSettings();
    this.ffmpegService.getMetaData(
      this.settingsService.path.join(
        this.settingsService.settings.storagePath,
        `${this.clip.id}.mp4`)
    ).pipe(
      tap((videoInfo) => {
        this.videoInfo = videoInfo;
      }),
      finalize(() => {
        this.isLoading = false;
        this.changeDetection.detectChanges();
      })
    ).subscribe();
  }

  get clipDuration(): string {
    const duration = moment.duration(this.videoInfo.format.duration, 'seconds');
    return `${duration.minutes().toFixed(0)}`
      + `:${duration.seconds().toFixed(0).padStart(2, '0')}`
      + `.${duration.milliseconds().toFixed(0).padStart(2, '0')}`;
  }
  get clipSize(): string {
    return `${(this.videoInfo.format.size / 1024).toFixed(0)} kB`;
  }
  get clipBitrate(): string {
    return `${(this.videoInfo.format.bit_rate / 1024).toFixed(0)} kbps`;
  }
  get clipResolution(): string {
    const videoStream = this.videoInfo.streams.filter(i => i.codec_type === 'video')[0];
    return `${videoStream.width} x ${videoStream.height}`;
  }
}
