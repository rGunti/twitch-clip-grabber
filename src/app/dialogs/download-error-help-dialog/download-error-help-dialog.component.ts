import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './download-error-help-dialog.component.html',
  styleUrls: ['./download-error-help-dialog.component.css']
})
export class DownloadErrorHelpDialogComponent implements OnInit {

  helpfulError: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public readonly originalError: any
  ) {
    console.debug(JSON.stringify(originalError));
  }

  ngOnInit(): void {
    const originalError = this.originalError as string;
    if (originalError) {
      if (originalError.indexOf('ENOENT') >= 0) {
        this.helpfulError = 'While download your clip, youtube-dl could not be found. ' +
          'Please check, if you can find a youtube-dl executable (youtube-dl.exe) beside the ' +
          'Sir Grabbington executable in the same folder.';
      } else if (originalError.indexOf('3221225781') >= 0) {
        this.helpfulError = 'Windows is missing a critical dependency to run the downloading process. ' +
          'The application requires MSVCR100.dll to be present on the system. To fix this issue, ' +
          'download the "Microsoft Visual C++ 2010 Service Pack 1 Redistributable Package" from ' +
          'Microsoft\'s website (https://www.microsoft.com/en-US/download/details.aspx?id=26999), ' +
          'make sure to download the 32-bit version, install it and try again. If this doesn\'t work, ' +
          'reboot your computer and try again.';
      }
    }

    if (!this.helpfulError) {
      this.helpfulError = 'I currently don\'t know this error yet so I can\'t provide you with more details.';
    }
  }

}
