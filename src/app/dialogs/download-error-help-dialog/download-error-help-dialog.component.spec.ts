import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadErrorHelpDialogComponent } from './download-error-help-dialog.component';

describe('DownloadErrorHelpDialogComponent', () => {
  let component: DownloadErrorHelpDialogComponent;
  let fixture: ComponentFixture<DownloadErrorHelpDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadErrorHelpDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadErrorHelpDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
