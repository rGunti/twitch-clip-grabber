import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipProcessComponent } from './clip-process.component';

describe('ClipProcessComponent', () => {
  let component: ClipProcessComponent;
  let fixture: ComponentFixture<ClipProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClipProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
