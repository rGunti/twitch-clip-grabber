import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HelixClipData } from 'twitch/lib/API/Helix/Clip/HelixClip';
import { FfmpegService, SettingsService, RenderProgress } from '../../core/services';
import { concat } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: './clip-process.component.html',
  styleUrls: ['./clip-process.component.css']
})
export class ClipProcessComponent implements OnInit {

  mode = 'indeterminate';
  status = 'Initializing …';
  percentage: number;

  currentJob: number;

  get totalJobs(): number {
    return this.clips.length;
  }

  constructor(
    private readonly dialogRef: MatDialogRef<ClipProcessComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly clips: HelixClipData[],
    private readonly ffmpeg: FfmpegService,
    private readonly settingsService: SettingsService,
    private readonly changeDetection: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.settingsService.loadSettings();

    const totalClips = this.clips.length;
    let i = 0;

    concat(
      ...this.clips.map(clip => {
        i += 1;
        const index = i;
        return this.ffmpeg.rescale(
          this.settingsService.path.join(this.settingsService.settings.storagePath, `${clip.id}.mp4`),
          1280, 720
        ).pipe(
          map(stats => {
            return { info: stats, clip: clip, index: index }
          })
        );
      })
    ).subscribe(
      (p) => {
        console.log('Rendering status update', p);
        if (p.info.stats) {
          this.percentage = p.info.stats.percent;
          this.mode = 'determinate';
        } else {
          this.percentage = 0;
          this.mode = 'indeterminate';
        }
        this.currentJob = p.index;
        this.status = `${p.index}/${totalClips}: ${p.info.statusText}`;
        this.changeDetection.detectChanges();
      }, (e: Error) => {
        console.error('Processing failed!', e);
        if (e.message.indexOf('ENOENT') >= 0) {
          alert(`Processing of videos has failed because ffmpeg could not be found on your system.`);
        } else {
          alert(`Processing failed! ${e.message}`);
        }
        this.dialogRef.close();
      }, () => {
        console.log('Rendering completed');
        this.dialogRef.close();
        this.changeDetection.detectChanges();
      }
    );
  }

}
