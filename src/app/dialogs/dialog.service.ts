import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ComponentType } from '@angular/cdk/portal';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    private matDialog: MatDialog
  ) { }

  open<T, D>(
    dialog: ComponentType<T>,
    data?: D,
    disableClose: boolean = true,
    width?: string, height?: string,
    minWidth: string = '400px', minHeight?: string,
    maxWidth?: string, maxHeight?: string,
    panelClass?: string
  ): MatDialogRef<T, D> {
    return this.matDialog.open(dialog, {
      data,
      disableClose,
      width, height,
      minWidth, minHeight,
      maxWidth, maxHeight,
      panelClass
    });
  }

  openAndAwait<T, D>(
    dialog: ComponentType<T>,
    data?: D,
    disableClose: boolean = true,
    width?: string, height?: string,
    minWidth?: string, minHeight?: string,
    maxWidth?: string, maxHeight?: string,
    panelClass?: string
  ): Promise<D> {
    const dialogRef = this.open(dialog, data, disableClose,
      width, height, minWidth, minHeight, maxWidth, maxHeight,
      panelClass);
    return dialogRef.afterClosed().toPromise();
  }
}
