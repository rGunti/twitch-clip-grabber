const fs = require('fs');

console.log('Loading changelog file ...');
const changelog = fs.readFileSync('CHANGELOG.md').toString();

const changelogFile = `
/* ****************************************** */
/* CHANGE LOG CODE FILE                       */
/* This file has been automatically generated */
/* during the build process and may not be    */
/* commited to source control                 */
/* ****************************************** */

export const CHANGELOG: string = ${JSON.stringify(changelog)};
`;

console.log('Writing generated changelog file ...');
fs.writeFileSync('src/app/shared/generated/changelog.ts', changelogFile);
